import gradio as gr
import requests
import os

API_URL = os.getenv("API_URL")

def predict_survival(pclass, sex, age, sibsp, parch, fare, embarked):
    if pclass not in ["1", "2", "3"]:
        return "Passenger Class must be between 1 and 3"
    if sex not in ["Male", "Female"]:
        return "Sex must be either 'Male' or 'Female'"
    if not (0 <= age <= 100):
        return "Age must be between 0 and 100"
    if not (0 <= sibsp <= 10):
        return "Number of Siblings/Spouses Aboard must be between 0 and 10"
    if not (0 <= parch <= 10):
        return "Number of Parents/Children Aboard must be between 0 and 10"
    if not (0 <= fare <= 500):
        return "Fare must be between 0 and 500"
    if embarked not in ["Cherbourg", "Queenstown", "Southampton"]:
        return "Port of Embarkation must be 'Cherbourg', 'Queenstown', or 'Southampton'"

    pclass = int(pclass)
    sex = 1 if sex == "Male" else 0
    embarked = ["Cherbourg", "Queenstown", "Southampton"].index(embarked)

    features = [pclass, sex, age, sibsp, parch, fare, embarked]
    response = requests.post(API_URL, json={"features": features})
    if response.status_code == 200:
        prediction = response.json()["prediction"]
        if prediction == 1:
            return "You would have survived"
        else:
            return "You would not have survived"
    else:
        return f"Error: {response.json()}"

iface = gr.Interface(
    fn=predict_survival,
    inputs=[
        gr.Dropdown(label="Passenger Class", choices=["1", "2", "3"]),
        gr.Dropdown(label="Sex", choices=["Female", "Male"]),
        gr.Number(label="Age", minimum=0, maximum=100, step=0.1),
        gr.Number(label="Number of Siblings/Spouses Aboard", minimum=0, maximum=10, step=1),
        gr.Number(label="Number of Parents/Children Aboard", minimum=0, maximum=10, step=1),
        gr.Number(label="Fare", minimum=0, maximum=500, step=0.01),
        gr.Dropdown(label="Port of Embarkation", choices=["Cherbourg", "Queenstown", "Southampton"])
    ],
    outputs=gr.Textbox(label="Survival Prediction Result"),
    title="Titanic Survival Prediction",
    description="Enter your data and click 'Submit' to find out if you would have survived",
    live=False,
    flagging_options=[]
)

if __name__ == "__main__":
    iface.launch(server_name="0.0.0.0", server_port=8888)
