.PHONY: docker-login docker deploy-service

PROJECT           ?= model-serving

CONTEXT           ?= .
DOCKERFILE        ?= $(CONTEXT)/Dockerfile
DOCKER_REGISTRY   ?= itmo-mlops.autospecml.online:8082
DOCKER_TAG        ?= v1.0.0
IMAGE             ?= $(DOCKER_REGISTRY)/$(PROJECT)
DOCKER_UID        ?= $(shell id -u)
DOCKER_GID        ?= $(shell id -g)
DOCKER_NAME       ?= $(shell id -u -n)

SERVICE           ?= $(PROJECT).service

ifdef NO_CACHE
DOCKER_BUILD_OPTS += --no-cache
endif

docker-login:
	@echo "======================================="
	@echo "Login to the docker registry ..."
	@echo "======================================="
	@docker login -u $(DOCKER_USER) -p $(DOCKER_PASS) $(DOCKER_REGISTRY)

docker: $(DOCKERFILE) docker-login
	@echo "======================================="
	@echo "Build the docker image ..."
	@echo "======================================="
	docker build $(DOCKER_BUILD_OPTS) \
		--file "$(DOCKERFILE)" \
		--build-arg "DOCKER_UID=$(DOCKER_UID)" \
		--build-arg "DOCKER_GID=$(DOCKER_GID)" \
		--build-arg "DOCKER_NAME=$(DOCKER_NAME)" \
		--tag "$(IMAGE):$(DOCKER_TAG)" \
		$(CONTEXT)
	docker push "$(IMAGE):$(DOCKER_TAG)"

deploy-service:
	sudo bash -c \
	'systemctl stop "$(SERVICE)" || true; \
	rm -rf "/opt/$(PROJECT)" && \
	mkdir -p "/opt/$(PROJECT)" && \
	cp "$(ENV_FILE)" "/opt/$(PROJECT)/.env" && \
	cp docker-compose.yml "/opt/$(PROJECT)" && \
	cp "$(SERVICE)" /etc/systemd/system/ && \
	systemctl daemon-reload && \
	systemctl enable "$(SERVICE)" && \
	systemctl start "$(SERVICE)"'
