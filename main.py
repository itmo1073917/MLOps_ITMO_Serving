from fastapi import FastAPI, HTTPException, Request
from pydantic import BaseModel
import mlflow.pyfunc
import pandas as pd
import os
import time
from prometheus_client import start_http_server, Summary, Counter, Gauge
import uvicorn

app = FastAPI()

model_name = os.getenv("MLFLOW_MODEL_NAME")
model_version = os.getenv("MLFLOW_MODEL_VERSION")
retry_attempts = int(os.getenv("MLFLOW_RETRY_ATTEMPTS", 5))
retry_interval = int(os.getenv("MLFLOW_RETRY_INTERVAL", 5))

# Prometheus metrics
REQUEST_COUNT = Counter("request_count", "Total number of requests")
EXCEPTION_COUNT = Counter("exception_count", "Total number of exceptions")
REQUEST_TIME = Summary("request_processing_seconds", "Time spent processing request")
MODEL_LOAD_TIME = Summary("model_load_time", "Time spent loading the model")
MODEL_HEALTH = Gauge("model_health", "Health of the model")

model = None
with MODEL_LOAD_TIME.time():
    for attempt in range(retry_attempts):
        try:
            model = mlflow.pyfunc.load_model(model_uri=f"models:/{model_name}/{model_version}")
            break
        except Exception as e:
            print(f"Attempt {attempt + 1} failed: {e}")
            if attempt < retry_attempts - 1:
                time.sleep(retry_interval)
            else:
                raise RuntimeError("Failed to load model after multiple attempts")

class PredictionRequest(BaseModel):
    features: list[float]

class PredictionResponse(BaseModel):
    prediction: float

@app.post("/predict", response_model=PredictionResponse)
@REQUEST_TIME.time()
def predict(request: PredictionRequest):
    REQUEST_COUNT.inc()
    try:
        features = pd.DataFrame([request.features])
        prediction = model.predict(features)
        return PredictionResponse(prediction=prediction[0])
    except Exception as e:
        EXCEPTION_COUNT.inc()
        raise HTTPException(status_code=500, detail=str(e))

@app.get("/health")
def health_check():
    try:
        test_features = pd.DataFrame([[1, 0, 3, 25.0, 0, 0, 7.25]])
        model.predict(test_features)
        MODEL_HEALTH.set(1)
        return {"status": "healthy"}
    except Exception:
        MODEL_HEALTH.set(0)
        raise HTTPException(status_code=500, detail="Model is not available")

@app.on_event("startup")
def startup_event():
    start_http_server(8001)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
