FROM python:3.9-slim

WORKDIR /app

RUN apt update && apt install curl gcc python3-dev -y

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]
