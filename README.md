## Serving Model Project

This project provides a Dockerized Serving model environment with GUI service.

**Getting Started**

1. Clone the repository:
    ```sh
    git clone https://gitlab.com/itmo1073917/MLOps_ITMO_Serving/
    ```
2. Create a `.env` file with your environment variables (see `.env_example` file for example)
3. Run this command to start the containers
   ```sh
   docker compose up
   ```
4. Access GUI at `http://localhost/`

**License**

This project is licensed under the [MIT License](https://opensource.org/licenses/MIT).
